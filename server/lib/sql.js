let config = {
	host: '127.0.0.1',
	user: 'alp_access',
	password: 'alp_password',
	database: 'ALP_DB',
	multipleStatements: true,
	dateStrings: ['DATE', 'DATETIME'],
}
module.exports = config
