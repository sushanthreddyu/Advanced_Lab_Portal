-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: ALP_DB
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_table`
--

DROP TABLE IF EXISTS `admin_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_table` (
  `username` varchar(20) NOT NULL,
  `password` char(60) DEFAULT NULL,
  `f_name` varchar(20) DEFAULT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `phone` decimal(10,0) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_table`
--

LOCK TABLES `admin_table` WRITE;
/*!40000 ALTER TABLE `admin_table` DISABLE KEYS */;
INSERT INTO `admin_table` VALUES ('abc123','$2b$10$YwmixVirZ.Ze4jEQPYo/DObRtzrYWzvfPiQaYdWrRRhmxHA01FCsS','first','last',1234567890,'abc@gmail.com','male','superadmin','mlrit');
/*!40000 ALTER TABLE `admin_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assigned_labs`
--

DROP TABLE IF EXISTS `assigned_labs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigned_labs` (
  `fid` int(11) DEFAULT NULL,
  `lab_id` int(11) DEFAULT NULL,
  KEY `fid` (`fid`),
  KEY `lab_id` (`lab_id`),
  CONSTRAINT `assigned_labs_ibfk_1` FOREIGN KEY (`fid`) REFERENCES `faculty_table` (`fid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assigned_labs_ibfk_2` FOREIGN KEY (`lab_id`) REFERENCES `lab_table` (`lab_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigned_labs`
--

LOCK TABLES `assigned_labs` WRITE;
/*!40000 ALTER TABLE `assigned_labs` DISABLE KEYS */;
INSERT INTO `assigned_labs` VALUES (1,1);
/*!40000 ALTER TABLE `assigned_labs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_table`
--

DROP TABLE IF EXISTS `course_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_table` (
  `name` varchar(60) DEFAULT NULL,
  `dept` varchar(4) DEFAULT NULL,
  `sem` decimal(1,0) DEFAULT NULL,
  `prog` varchar(6) DEFAULT NULL,
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_table`
--

LOCK TABLES `course_table` WRITE;
/*!40000 ALTER TABLE `course_table` DISABLE KEYS */;
INSERT INTO `course_table` VALUES ('C++','CSE',3,'B.Tech',1);
/*!40000 ALTER TABLE `course_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty_table`
--

DROP TABLE IF EXISTS `faculty_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty_table` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) DEFAULT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `password` char(60) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `phone` decimal(10,0) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dept` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty_table`
--

LOCK TABLES `faculty_table` WRITE;
/*!40000 ALTER TABLE `faculty_table` DISABLE KEYS */;
INSERT INTO `faculty_table` VALUES (1,'first','last','$2b$10$NTMXaHayhYsxdNpjxNGCdOj8q1Jhc02f/im3GG16vrV.PydGJAG9q','male',1234567890,'abc@gmail.com','CSE');
/*!40000 ALTER TABLE `faculty_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_info_table`
--

DROP TABLE IF EXISTS `lab_info_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab_info_table` (
  `lab_id` int(11) DEFAULT NULL,
  `day` varchar(10) DEFAULT NULL,
  `slot` decimal(1,0) DEFAULT NULL,
  KEY `lab_id` (`lab_id`),
  KEY `slot` (`slot`),
  CONSTRAINT `lab_info_table_ibfk_1` FOREIGN KEY (`lab_id`) REFERENCES `lab_table` (`lab_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lab_info_table_ibfk_2` FOREIGN KEY (`slot`) REFERENCES `lab_slots` (`slot`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_info_table`
--

LOCK TABLES `lab_info_table` WRITE;
/*!40000 ALTER TABLE `lab_info_table` DISABLE KEYS */;
INSERT INTO `lab_info_table` VALUES (1,'wed',1);
/*!40000 ALTER TABLE `lab_info_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_slots`
--

DROP TABLE IF EXISTS `lab_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab_slots` (
  `slot` decimal(1,0) NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  PRIMARY KEY (`slot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_slots`
--

LOCK TABLES `lab_slots` WRITE;
/*!40000 ALTER TABLE `lab_slots` DISABLE KEYS */;
INSERT INTO `lab_slots` VALUES (1,'10:00:00','12:30:00'),(2,'01:20:00','03:50:00');
/*!40000 ALTER TABLE `lab_slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_student_table`
--

DROP TABLE IF EXISTS `lab_student_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab_student_table` (
  `lab_id` int(11) DEFAULT NULL,
  `rollno` varchar(10) DEFAULT NULL,
  KEY `lab_id` (`lab_id`),
  KEY `rollno` (`rollno`),
  CONSTRAINT `lab_student_table_ibfk_1` FOREIGN KEY (`lab_id`) REFERENCES `lab_table` (`lab_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lab_student_table_ibfk_2` FOREIGN KEY (`rollno`) REFERENCES `student_table` (`rollno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_student_table`
--

LOCK TABLES `lab_student_table` WRITE;
/*!40000 ALTER TABLE `lab_student_table` DISABLE KEYS */;
INSERT INTO `lab_student_table` VALUES (1,'17R21A05P3'),(1,'1R21A05P3');
/*!40000 ALTER TABLE `lab_student_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_table`
--

DROP TABLE IF EXISTS `lab_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lab_table` (
  `lab_id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_year` year(4) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`lab_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `lab_table_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course_table` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_table`
--

LOCK TABLES `lab_table` WRITE;
/*!40000 ALTER TABLE `lab_table` DISABLE KEYS */;
INSERT INTO `lab_table` VALUES (1,2019,1);
/*!40000 ALTER TABLE `lab_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,'program'),(2,'mcq'),(3,'blanks'),(4,'essay');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_table`
--

DROP TABLE IF EXISTS `session_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_table` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_id` int(11) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `prob_set` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `lab_id` (`lab_id`),
  CONSTRAINT `session_table_ibfk_1` FOREIGN KEY (`lab_id`) REFERENCES `lab_table` (`lab_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_table`
--

LOCK TABLES `session_table` WRITE;
/*!40000 ALTER TABLE `session_table` DISABLE KEYS */;
INSERT INTO `session_table` VALUES (3,1,1,'2019-11-02','session_3.json','Pending'),(4,1,1,'2019-11-02','session_4.json','Pending'),(5,1,1,'2019-11-02','session_5','Pending'),(6,1,1,'2019-11-02','session_6','Pending');
/*!40000 ALTER TABLE `session_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_table`
--

DROP TABLE IF EXISTS `student_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_table` (
  `rollno` varchar(10) NOT NULL,
  `password` char(60) DEFAULT NULL,
  `f_name` varchar(20) DEFAULT NULL,
  `l_name` varchar(20) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `phone` decimal(10,0) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `parent_phone` decimal(10,0) DEFAULT NULL,
  `parent_email` varchar(50) DEFAULT NULL,
  `dept` varchar(4) DEFAULT NULL,
  `year` decimal(1,0) DEFAULT NULL,
  `section` char(1) DEFAULT NULL,
  `prog` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`rollno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_table`
--

LOCK TABLES `student_table` WRITE;
/*!40000 ALTER TABLE `student_table` DISABLE KEYS */;
INSERT INTO `student_table` VALUES ('17R21A05P3','$2b$10$D6y276oRK4a8ucG6sPg5k.FbhsJRoPZTjf283PBNOtDv2HZOhtawS','first','last','male',1234567890,'abc@gmail.com',1233838,'daddy@hotmail.com','CSE',1,'D','B.Tech'),('1R21A05P3','$2b$10$CttHxK7970g7PVJPmFss/OV0g5umaSJDRVrl37XnxZ935A8Hinp8K','first','last','male',1234567890,'abc@gmail.com',1233838,'daddy@hotmail.com','CSE',1,'D','B.Tech');
/*!40000 ALTER TABLE `student_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submissions_table`
--

DROP TABLE IF EXISTS `submissions_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submissions_table` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `rollno` varchar(10) DEFAULT NULL,
  `sub_no` int(11) DEFAULT NULL,
  `submission` varchar(100) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `session_id` (`session_id`),
  KEY `rollno` (`rollno`),
  CONSTRAINT `submissions_table_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `session_table` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `submissions_table_ibfk_2` FOREIGN KEY (`rollno`) REFERENCES `student_table` (`rollno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submissions_table`
--

LOCK TABLES `submissions_table` WRITE;
/*!40000 ALTER TABLE `submissions_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `submissions_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-17 15:19:28
